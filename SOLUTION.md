# Video System
## Overview
There are two parts, a web client and a server.

## Features
- View all videos in the list
- Add a video to the list
- Edit a video (Click on the row):
    - Not allowed to update the videoId because an id should be unique and immutable.
    - Not allowed to enter a negative id.
    - Not allowed to enter a negative duration.
- Delete a video from the list
- Watch a video

However, users are not allowed to update the videoId because an id should be unique and immutable.

## Technologies
In general, I added the following frameworks and packages.

- Redux
- ReactPlayer
- ReactTable
- react-md
- axios
- webpack-dev-server (hot reload)
- Other libraries for webpack and development

## Backend
I added some methods for api requests which include request checking. The methods either receive or return Json format DTO. If the request is not expected, it will be rejected.

## Frontend
The most important part is in the *AppContainer.jsx* which is on top of other components. The redux actions will send requests to the server, and there is a redux data store containing all the messages received from redux reducers which include videos and routing history. I used webpack-dev-server to facilitate the development process.

## Run
```bash
$ cd <client_location> && npm i && npm run build
$ cd <server_location> && ./gradlew appRun
```

and for client hot reload

```bash
$ cd <client_location> && npm start
```

## Local Port
- ./gradlew appRun: http://localhost:8080
- npm start: http://localhost:3000

## Improvement
If I have more time, I would love to do the following tasks.

- Database: Generating a docker based local database for development.
- Docker: Containerise all the services.
- Integration: Integrate gradle with webpack.
- Auto-detect Video Duration: Capture the actual video duration from the src automatically instead of editing ourselves.
- Video Browser: The ability of browsing the videos from other websites such as YouTube and adding the video to our list.
- Deployment: Deploy docker images onto AWS ECS.