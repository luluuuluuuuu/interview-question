const webpack = require('webpack')
const path = require('path')

//variables
const isProduction = process.argv.indexOf('-p') >= 0
const sourcePath = path.join(__dirname, './src')
const outPath = path.join(__dirname, '../server/src/main/webapp/resources/scripts')

//plugins
const HtmlWebPackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    entry: {
        main: './src/index.js',
        vendor: [
            'react',
            'react-dom',
            'react-redux',
            'react-router',
            'redux',
            'babel-polyfill'
        ]
    },
    output: {
        path: outPath,
        filename: 'bundle.js',
        chunkFilename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js(x)?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader'
                }]
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader',
                        query: {
                            modules: true,
                            sourceMap: !isProduction,
                            importLoaders: 1,
                            localIdentName: '[local]__[hash:base64:5]'
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                require('postcss-import')({
                                    addDependencyTo: webpack
                                }),
                                require('postcss-url')(),
                                require('postcss-cssnext')(),
                                require('postcss-reporter')(),
                                require('postcss-browser-reporter')({
                                    disabled: isProduction
                                }),
                            ]
                        }
                    }
                    ]
                })
            },
            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    performance: {
        hints: isProduction ? false : 'warning'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    plugins: [
        new webpack.optimize.AggressiveMergingPlugin(),
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: isProduction ? '../../index.html' : 'index.html'
        }),
        new ExtractTextPlugin({
            filename: 'styles.css',
            disable: !isProduction
        })
    ],
    resolve: {
        extensions: ['.js', '.json', '.jsx']
    },
    devServer: {
        contentBase: sourcePath,
        hot: true,
        stats: {
            warnings: false
        },
        proxy: {
            '/api/*': {
                target: 'http://localhost:8080',
                secure: false
            }
        }
    }
}