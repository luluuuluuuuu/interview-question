import axios from 'axios'
import { VideoTypes } from '../action-types'

export const listVideos = () =>
    async (dispatch, getState) =>
        axios.get('/api/videos')
            .then(response => {
                dispatch({
                    type: VideoTypes.VIDEOS_LISTED,
                    payload: response.data
                })
            })
            .catch(error => {
                console.log(error)
            })

export const addVideo = (video) =>
    async (dispatch, getState) =>
        axios.post('/api/videos/add', video)
            .then(response => {
                dispatch({
                    type: VideoTypes.VIDEO_ADDED,
                    payload: response.data
                })
                dispatch(listVideos())
            })
            .catch(error => {
                console.log(error)
            })

export const removeVideo = (videoId) =>
    async (dispatch, getState) =>
        axios.delete('/api/videos/delete?id=' + videoId)
            .then(response => {
                dispatch({
                    type: VideoTypes.VIDEO_REMOVED,
                    payload: response.data
                })
                dispatch(listVideos())
            })
            .catch(error => {
                console.log(error)
            })

export const updateVideo = (videoId, video) =>
    async (dispatch, getState) =>
        axios.put('/api/videos/update?id=' + videoId, video)
            .then(response => {
                dispatch({
                    type: VideoTypes.VIDEO_UPDATED,
                    payload: response.data
                })
                dispatch(listVideos())
            })
            .catch(error => {
                console.log(error)
            })