import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducers'

export const configureStore = (history, initialState) => {

    const create = window.devToolsExtension
        ? window.devToolsExtension()(createStore)
        : createStore

    const router = routerMiddleware(history)
    const createStoreWithMiddleware = applyMiddleware(router, thunk)(create)
    const store = createStoreWithMiddleware(rootReducer, initialState)

    return store
}
