import React from 'react'
import { Button, DialogContainer } from 'react-md'
import DialogForm from '../DialogForm'

export default class Dialog extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const actions = []
        if (this.props.dialogMode === 'UPDATE') {
            actions.push(<Button flat primary onClick={this.props.updateVideo.bind(this, this.props.video.videoId)}>Update</Button>)
            actions.push(<Button flat secondary onClick={this.props.removeVideo.bind(this, this.props.video.videoId)}>Delete</Button>)
            actions.push(<Button flat secondary onClick={this.props.hide}>Cancel</Button>)
        } else {
            actions.push(<Button flat primary onClick={this.props.addVideo}>Add</Button>)
            actions.push(<Button flat secondary onClick={this.props.hide}>Cancel</Button>)
        }

        return (
            <div>
                <DialogContainer
                    visible={this.props.visible}
                    onHide={this.props.hide}
                    actions={actions}
                    title='Video Information'
                >
                    <DialogForm
                        isIdEditable={this.props.dialogMode === 'ADD'}
                        video={this.props.video}
                        handleFieldChange={this.props.handleFieldChange.bind(this)}
                    />
                </DialogContainer>
            </div>
        )
    }
}