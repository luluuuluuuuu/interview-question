import React from 'react'

export const videoTableColumns = [
    {
        Header: 'Video Id',
        id: 'videoId',
        accessor: 'videoId'
    },
    {
        Header: 'Video Name',
        id: 'videoName',
        accessor: 'videoName'
    },
    {
        Header: 'Duration',
        id: 'duration',
        accessor: 'duration'
    },
    {
        Header: 'Source',
        id: 'src',
        accessor: 'src'
    }
]