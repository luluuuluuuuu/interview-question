import React from 'react'
import ReactTable from 'react-table'
import { Button } from 'react-md'
import './style.scss'

export default class Table extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const columnDefs = [
            ...this.props.columnDefs,
            {
                Header: 'Watch',
                id: 'watch',
                accessor: 'src',
                Cell: row => (
                    <i className='material-icons watch-btn' onClick={this.props.setWatching.bind(this, row.original)}>
                        airplay
                    </i>
                )
            }
        ]

        const reactTable = <ReactTable
            className='-striped -highlight'
            data={this.props.data}
            columns={columnDefs}
            showPageSizeOptions={false}
            defaultPageSize={10}
            getTdProps={(state, rowInfo, column) => {
                let att = {
                    onClick: () => rowInfo && column.id !== 'watch' && this.props.onRowClick(rowInfo.original.videoId),
                    style: {
                        cursor: rowInfo && column.id !== 'watch' ? 'pointer' : 'auto',
                        textAlign: rowInfo && column.id !== 'watch' ? 'left' : 'center'
                    }
                }
                return att
            }}
        />

        return (
            <div>
                {reactTable}
            </div>)
    }
}