import { VideoTypes } from '../action-types'

const videoInitialState = []

export const videos = (state = videoInitialState, action) => {
    switch (action.type) {

        case VideoTypes.VIDEOS_LISTED:
            return [
                ...action.payload
            ]

        case VideoTypes.VIDEO_ADDED:
        case VideoTypes.VIDEO_REMOVED:
        case VideoTypes.VIDEO_UPDATED:
        default:
            return state
    }
}