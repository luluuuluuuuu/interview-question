import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'
import { videos } from './video-reducers'

export default combineReducers({
    videos,
    routing
})