import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button, Toolbar, Grid, Cell } from 'react-md'
import Player from 'react-player'
import { listVideos, addVideo, removeVideo, updateVideo } from '../actions'
import Dialog from '../Dialog'
import Table from '../Table'
import { videoTableColumns } from '../Table/column-defs'
import './style.scss'

class AppContainer extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedVideoId: -1,
            selectedVideoName: '',
            selectedVideoDuration: -1,
            selectedVideoSrc: '',
            dialogVisible: false,
            dialogMode: '',
            watchingVideo: null
        }
    }

    componentWillMount() {
        this.props.listVideos()
    }

    addVideo() {
        const request = this.mapStateToUpdateRequest(this.state)
        if (!this.isValidInput(request)) {
            throw 'Invalid input'
        }

        this.props.addVideo(request)
        this.hideDialog()
    }

    removeVideo(videoId) {
        this.props.removeVideo(videoId)
        if (this.state.watchingVideo && videoId === this.state.watchingVideo.videoId) {
            this.setWatchingVideo(null)
        }
        this.hideDialog()
    }

    updateVideo(videoId) {
        const request = this.mapStateToUpdateRequest(this.state)
        if (!this.isValidInput(request)) {
            throw 'Invalid input'
        }
        if (this.state.watchingVideo && videoId === this.state.watchingVideo.videoId) {
            this.setWatchingVideo(request)
        }

        this.props.updateVideo(videoId, request)
        this.hideDialog()
    }

    setWatchingVideo(video) {
        this.setState({
            watchingVideo: video
        })
    }

    mapStateToUpdateRequest(state) {
        const request = {
            videoId: state.selectedVideoId,
            videoName: state.selectedVideoName,
            duration: state.selectedVideoDuration,
            src: state.selectedVideoSrc
        }

        return request
    }

    isValidInput(request) {
        return request
            && request.videoId
            && request.videoName
            && request.duration
            && request.src
            && request.videoId >= 0
            && request.duration >= 0
    }

    handleFieldChange(key, value) {
        this.setState({
            [key]: value
        })
    }

    showDialog(dialogMode, videoId) {
        if (dialogMode === 'UPDATE') {
            const selectedVideo = this.props.videos.find(video => video.videoId === videoId)
            this.setState({
                selectedVideoId: videoId,
                selectedVideoName: selectedVideo.videoName,
                selectedVideoDuration: selectedVideo.duration,
                selectedVideoSrc: selectedVideo.src
            })
        } else {
            this.setState({
                selectedVideoId: '',
                selectedVideoName: '',
                selectedVideoDuration: 0,
                selectedVideoSrc: ''
            })
        }

        this.setState({
            dialogMode,
            dialogVisible: true
        })
    }

    hideDialog() {
        this.setState({
            selectedVideoId: -1,
            selectedVideoName: '',
            selectedVideoDuration: -1,
            selectedVideoSrc: '',
            dialogVisible: false
        })
    }

    render() {
        const video = {
            videoId: this.state.selectedVideoId,
            videoName: this.state.selectedVideoName,
            duration: this.state.selectedVideoDuration,
            src: this.state.selectedVideoSrc
        }

        return (
            <div className='container'>
                <Toolbar
                    themed
                    className='top-bar'
                    title='My Video List'
                />
                <Grid className='dashboard'>
                    <Cell size={6} className='table-block'>
                        <Button raised primary className='add-btn' onClick={this.showDialog.bind(this, 'ADD')}>New Video</Button>
                        <Table
                            data={this.props.videos}
                            columnDefs={videoTableColumns}
                            onRowClick={this.showDialog.bind(this, 'UPDATE')}
                            setWatching={this.setWatchingVideo.bind(this)}
                        />
                    </Cell>
                    <Cell size={6} className='video-player-block'>
                        <h3 className='video-title'>{this.state.watchingVideo ? this.state.watchingVideo.videoName : 'No Video Playing'}</h3>
                        <Player
                            className='video-player'
                            playing
                            controls
                            url={this.state.watchingVideo && this.state.watchingVideo.src}
                            height='88%'
                            width='100%'
                        />
                    </Cell>
                </Grid>
                <div className='bottom-bar'>
                    <Grid>
                        <Cell size={3}>
                            <p className='hint-text'>Click on the row to edit</p>
                        </Cell>
                        <Cell size={1} offset={8}>
                            <p>- Built by Ken Lu</p>
                        </Cell>
                    </Grid>
                </div>
                <Dialog
                    dialogMode={this.state.dialogMode}
                    visible={this.state.dialogVisible}
                    hide={this.hideDialog.bind(this)}
                    video={video}
                    handleFieldChange={this.handleFieldChange.bind(this)}
                    removeVideo={this.removeVideo.bind(this)}
                    updateVideo={this.updateVideo.bind(this)}
                    addVideo={this.addVideo.bind(this)}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        videos: state.videos
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        listVideos: bindActionCreators(listVideos, dispatch),
        addVideo: bindActionCreators(addVideo, dispatch),
        removeVideo: bindActionCreators(removeVideo, dispatch),
        updateVideo: bindActionCreators(updateVideo, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)