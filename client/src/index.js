import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route, Switch } from 'react-router'
import { ConnectedRouter } from 'react-router-redux'
import { createBrowserHistory } from 'history'
import { configureStore } from './store'
import AppContainer from './container'
import './_globals.scss'

require('babel-polyfill')

const history = createBrowserHistory()
const store = configureStore(history)
const wrapper = document.getElementById('container')

wrapper ?
    ReactDOM.render(<Provider store={store}>
        <ConnectedRouter history={history}>
            <div>
                <Switch>
                    <Route exact={true} path='/' component={AppContainer} />
                </Switch>
            </div>
        </ConnectedRouter>
    </Provider>, wrapper) :
    false