import React from 'react'
import { TextField } from 'react-md'
import './style.scss'

export default class DialogForm extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className='dialog-form'>
                <form>
                    <div className='md-grid'>
                        <div className='general-fields md-cell md-cell--12'>
                            <div className='input-container'>
                                <TextField
                                    label='Video Id'
                                    className='input number'
                                    type='number'
                                    min={0}
                                    required={true}
                                    fullWidth={true}
                                    disabled={!this.props.isIdEditable}
                                    errorText='Required'
                                    onChange={this.props.handleFieldChange.bind(this, 'selectedVideoId')}
                                    value={this.props.video.videoId}
                                />
                            </div>
                            <div className='input-container'>
                                <TextField
                                    label='Video Name'
                                    className='input text'
                                    required={true}
                                    fullWidth={true}
                                    disabled={false}
                                    errorText='Required'
                                    onChange={this.props.handleFieldChange.bind(this, 'selectedVideoName')}
                                    value={this.props.video.videoName}
                                />
                            </div>
                            <div className='input-container'>
                                <TextField
                                    label='Duration'
                                    className='input number'
                                    type='number'
                                    min={0}
                                    required={true}
                                    fullWidth={true}
                                    disabled={false}
                                    errorText='Required'
                                    onChange={this.props.handleFieldChange.bind(this, 'selectedVideoDuration')}
                                    value={this.props.video.duration}
                                />
                            </div>
                            <div className='input-container'>
                                <TextField
                                    label='Source'
                                    className='input text'
                                    required={true}
                                    fullWidth={true}
                                    disabled={false}
                                    errorText='Required'
                                    onChange={this.props.handleFieldChange.bind(this, 'selectedVideoSrc')}
                                    value={this.props.video.src}
                                />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}