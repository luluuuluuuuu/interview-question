package com.smartzer;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.smartzer.controllers.VideosController;

public class ServerApplication extends Application {

	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> emptySet = new HashSet<Class<?>>();

	public ServerApplication(){
		this.singletons.add(new VideosController());
	}

	@Override
	public Set<Class<?>> getClasses() {
		return this.emptySet;
	}

	@Override
	public Set<Object> getSingletons() {
		return this.singletons;
	}

}
