package com.smartzer.controllers;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.smartzer.beans.Video;

@Path("/videos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class VideosController {
	private static Map<Long, Video> products = new HashMap<>();

	static {
		products.put(1L, new Video(1, "Sintel Trailer", 52, "https://media.w3.org/2010/05/sintel/trailer_hd.mp4"));
		products.put(2L, new Video(2, "Maroon 5 - Girls Like You ft. Cardi B", 270, "https://www.youtube.com/watch?v=aJOTlE1K90k"));
	}

	@GET
	@Path("/")
	public Response getVideos(@QueryParam("id") Long videoId) {
		List<Video> selectedVideos =
				videoId == null ?
						new ArrayList<>(products.values()) :
						products.entrySet()
								.stream()
								.filter(video -> video.getKey().equals(videoId))
								.map(Map.Entry::getValue)
								.collect(Collectors.toList());

		return Response.status(200).entity(selectedVideos).build();
	}

	@POST
	@Path("/add")
	public Response setVideo(Video video) {
		if (video == null || video.getVideoId() < 0 || products.containsKey(video.getVideoId())) {
			return Response.status(400).build();
		}

		products.put(video.getVideoId(), video);
		return Response.status(201).build();
	}

	@DELETE
	@Path("/delete")
	public Response removeVideo(@QueryParam("id") Long videoId) {
		if (videoId == null || videoId < 0 || !products.containsKey(videoId)) {
			return Response.status(400).build();
		}

		products.remove(videoId);
		return Response.status(200).build();
	}

	@PUT
	@Path("/update")
	public Response updateVideo(@QueryParam("id") Long videoId, Video video) {
		if (videoId == null || videoId < 0 || !products.containsKey(videoId)) {
			return Response.status(400).build();
		}

		verifyVideoInfo(videoId, video);
		products.replace(videoId, video);
		return Response.status(200).build();
	}

	private void verifyVideoInfo(Long videoId, Video video) {
		if (video.getVideoName() == null) {
			video.setVideoName(products.get(videoId).getVideoName());
		}
		if (video.getSrc() == null) {
			video.setSrc(products.get(videoId).getSrc());
		}
		if (video.getDuration() <= 0) {
			video.setDuration(products.get(videoId).getDuration());
		}

		video.setVideoId(videoId);
	}

}
