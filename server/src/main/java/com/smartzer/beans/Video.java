package com.smartzer.beans;

public class Video {
	long videoId;
	String videoName;
	long duration;
	String src;
	
	protected Video() {
		
	}

	public Video(long videoId, String videoName, long duration, String src) {
		super();
		this.videoId = videoId;
		this.videoName = videoName;
		this.duration = duration;
		this.src = src;
	}
	
	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
	public long getVideoId() {
		return videoId;
	}
	public void setVideoId(long videoId) {
		this.videoId = videoId;
	}
	
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (duration ^ (duration >>> 32));
		result = prime * result + ((src == null) ? 0 : src.hashCode());
		result = prime * result + (int) (videoId ^ (videoId >>> 32));
		result = prime * result + ((videoName == null) ? 0 : videoName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Video other = (Video) obj;
		if (duration != other.duration)
			return false;
		if (src == null) {
			if (other.src != null)
				return false;
		} else if (!src.equals(other.src))
			return false;
		if (videoId != other.videoId)
			return false;
		if (videoName == null) {
			if (other.videoName != null)
				return false;
		} else if (!videoName.equals(other.videoName))
			return false;
		return true;
	}
	

}
